﻿using System;

namespace LiteLoader.Extensions
{
    public interface IExtensionContainer
    {
        /// <summary>
        /// Author of the Extension
        /// </summary>
        string Author { get; }

        /// <summary>
        /// Is Extension loaded
        /// </summary>
        bool IsLoaded { get; }

        /// <summary>
        /// Absolute path to the extension file
        /// </summary>
        string Location { get; }

        /// <summary>
        /// Name of the Extension
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Title of the Extension
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Version of the Extension
        /// </summary>
        Version Version { get; }
    }
}
