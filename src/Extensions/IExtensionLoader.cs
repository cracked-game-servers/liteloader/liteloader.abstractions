﻿namespace LiteLoader.Extensions
{
    public interface IExtensionLoader
    {
        /// <summary>
        /// The absolute path to the directory of where extensions are loaded
        /// </summary>
        string ExtensionsPath { get; }

        /// <summary>
        /// Will return true if the extension is loaded
        /// </summary>
        /// <param name="extName"></param>
        /// <returns></returns>
        bool IsLoaded(string extName);

        /// <summary>
        /// The name of the extension to load
        /// </summary>
        /// <param name="extName"></param>
        /// <returns></returns>
        IExtensionResult Load(string extName);

        /// <summary>
        /// Loads a extension from raw assembly data
        /// </summary>
        /// <param name="libraryData"></param>
        /// <param name="symbolsData"></param>
        /// <returns></returns>
        IExtensionResult Load(byte[] libraryData, byte[] symbolsData);

        /// <summary>
        /// Unloads a extension by name
        /// </summary>
        /// <param name="extName">Name of the extension</param>
        /// <returns></returns>
        IExtensionResult Unload(string extName);
    }
}
