﻿using System;
using System.Linq;
using System.Reflection;

namespace LiteLoader.Extensions
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
    public class ExtensionInfoAttribute : Attribute
    {
        /// <summary>
        /// Extension Author
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Startup Interface for the extension
        /// </summary>
        public Type Startup { get; set; }

        /// <summary>
        /// Extension Title
        /// </summary>
        public string Title { get; set; }

        public static ExtensionInfoAttribute Get(Assembly assembly)
        {
            ExtensionInfoAttribute attribute = assembly.GetCustomAttributes(typeof(ExtensionInfoAttribute), false).FirstOrDefault() as ExtensionInfoAttribute;
            if (attribute == null) attribute = new ExtensionInfoAttribute();

            AssemblyName name = assembly.GetName();

            if (string.IsNullOrEmpty(attribute.Title))
            {
                AssemblyTitleAttribute title = assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false).FirstOrDefault() as AssemblyTitleAttribute;

                if (title != null && !string.IsNullOrEmpty(title.Title))
                {
                    attribute.Title = title.Title;
                }
                else
                {
                    attribute.Title = name.Name;
                }
            }

            if (string.IsNullOrEmpty(attribute.Author))
            {
                AssemblyCompanyAttribute company = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false).FirstOrDefault() as AssemblyCompanyAttribute;

                if (company != null && !string.IsNullOrEmpty(company.Company))
                {
                    attribute.Author = company.Company;
                }
                else
                {
                    attribute.Author = company.Company;
                }
            }

            if (attribute.Startup == null)
            {
                Type[] types = assembly.GetTypes();

                for (int i = 0; i < types.Length; i++)
                {
                    Type type = types[i];
                    MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                    bool found = false;

                    for (int m = 0; m < methods.Length; m++)
                    {
                        MethodInfo method = methods[m];

                        if (!method.IsStatic) continue;

                        if (method.Name.Equals("LoadExtension", StringComparison.OrdinalIgnoreCase) || method.Name.Equals("UnloadExtension", StringComparison.OrdinalIgnoreCase))
                        {
                            attribute.Startup = type;
                            found = true;
                        }
                    }

                    if (found) break;
                }
            }
            return attribute;
        }
    }
}
