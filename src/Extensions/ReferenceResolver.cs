﻿using System.Reflection;

namespace LiteLoader.Extensions
{
    public abstract class ReferenceResolver
    {
        /// <summary>
        /// Attempts to resolve a reference
        /// </summary>
        /// <param name="caller">The assembly asking to resolve</param>
        /// <param name="name">The referance name requested</param>
        /// <returns></returns>
        public abstract Assembly Resolve(Assembly caller, AssemblyName name);
    }
}
