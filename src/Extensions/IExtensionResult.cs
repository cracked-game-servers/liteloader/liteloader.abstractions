﻿using LiteLoader.Results;

namespace LiteLoader.Extensions
{
    public interface IExtensionResult : IResult
    {
        /// <summary>
        /// Get the requested Extension from result
        /// </summary>
        IExtensionContainer Extension { get; }
    }
}
