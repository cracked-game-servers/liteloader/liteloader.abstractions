﻿using LiteLoader.Plugins;
using System;

namespace LiteLoader.Game
{
    /// <summary>
    /// A command instance
    /// </summary>
    public interface ICommand : IEquatable<ICommand>
    {
        /// <summary>
        /// Group.Name of the command
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// Name of this command
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Parent/Group of this command
        /// </summary>
        string Parent { get; }

        /// <summary>
        /// The plugin responsible for this command
        /// </summary>
        Plugin Source { get; }
    }
}
