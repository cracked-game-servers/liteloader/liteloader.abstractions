﻿using System;

namespace LiteLoader.Game
{
    /// <summary>
    /// Defines a command type
    /// </summary>
    [Flags]
    public enum CommandType
    {
        /// <summary>
        /// None or unknown
        /// </summary>
        None = 0,

        /// <summary>
        /// Chat command
        /// </summary>
        Chat = 1,

        /// <summary>
        /// Console command
        /// </summary>
        Console = 2,

        /// <summary>
        /// Both chat and console commands
        /// </summary>
        Both = Chat | Console
    }
}
