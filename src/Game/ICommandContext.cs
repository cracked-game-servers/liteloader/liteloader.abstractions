﻿using System.Collections.Generic;

namespace LiteLoader.Game
{
    /// <summary>
    /// A command execution context
    /// </summary>
    public interface ICommandContext
    {
        /// <summary>
        /// Arguments provided with the command
        /// </summary>
        IEnumerable<string> Arguments { get; }

        /// <summary>
        /// The command being executed
        /// </summary>
        ICommand Command { get; }

        /// <summary>
        /// The player executing the command
        /// </summary>
        IPlayer Executor { get; }

        /// <summary>
        /// Reply back to the executor
        /// </summary>
        /// <param name="message">The message or object to reply with</param>
        void Reply(object message);
    }
}
