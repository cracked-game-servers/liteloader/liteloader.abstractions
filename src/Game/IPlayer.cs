﻿using System;
using System.Globalization;
using System.Net;

namespace LiteLoader.Game
{
    public interface IPlayer : IEquatable<IPlayer>
    {
        /// <summary>
        /// Authentication data provided by the client
        /// </summary>
        byte[] AuthenticationToken { get; }

        /// <summary>
        /// Is player currently connected
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Player connection endpoint
        /// </summary>
        IPEndPoint EndPoint { get; }

        /// <summary>
        /// Player ID
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Player is admin
        /// </summary>
        bool IsAdmin { get; set; }

        /// <summary>
        /// Player is banned
        /// </summary>
        bool IsBanned { get; }

        /// <summary>
        /// Player language
        /// </summary>
        CultureInfo Language { get; set; }

        /// <summary>
        /// The last command type
        /// </summary>
        CommandType LastCommand { get; }

        /// <summary>
        /// Player Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// The network protocol used to join
        /// </summary>
        uint NetworkProtocol { get; }

        /// <summary>
        /// Position referenece
        /// </summary>
        IPosition Position { get; }

        /// <summary>
        /// Bans this player with reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        void Ban(string reason = null, TimeSpan? duration = null);

        /// <summary>
        /// Sends a chat message to the player
        /// </summary>
        /// <param name="message"></param>
        void ChatMessage(string message);

        /// <summary>
        /// Kicks this player with reason
        /// </summary>
        /// <param name="reason"></param>
        void Kick(string reason = null);

        /// <summary>
        /// Replies to this player at it's last command source
        /// </summary>
        /// <param name="message"></param>
        void Reply(string message);

        /// <summary>
        /// Unban this player
        /// </summary>
        void Unban();
    }
}
