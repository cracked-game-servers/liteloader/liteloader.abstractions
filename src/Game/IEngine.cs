﻿using System;

namespace LiteLoader.Game
{
    public interface IEngine
    {
        /// <summary>
        /// Name of the current Engine
        /// </summary>
        string Engine { get; }

        /// <summary>
        /// Current time provided by the engine
        /// </summary>
        float EngineTime { get; }

        /// <summary>
        /// Executes a callback after a set delay
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <param name="callback"></param>
        void Delay(TimeSpan timeSpan, Action callback);

        /// <summary>
        /// Executes a callback every set interval
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        IDisposable Every(TimeSpan timeSpan, Action callback);

        /// <summary>
        /// Executes a callback next engine update
        /// </summary>
        /// <param name="callback"></param>
        void NextUpdate(Action callback);
    }
}
