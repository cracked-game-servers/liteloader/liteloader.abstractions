﻿using LiteLoader.Plugins;

namespace LiteLoader.Game
{
    /// <summary>
    /// A callback used for command executions
    /// </summary>
    /// <param name="context"></param>
    public delegate void CommandCallback(ICommandContext context);

    /// <summary>
    /// Interface for command integration
    /// </summary>
    public interface ICommandHandler
    {
        /// <summary>
        /// Registers a command with the command handler
        /// </summary>
        /// <param name="command">The command name to register</param>
        /// <param name="callback">The callback to call on command execution</param>
        /// <param name="plugin">The plugin this command is registered to</param>
        /// <returns>True on success</returns>
        bool RegisterCommand(string command, CommandCallback callback, Plugin plugin = null);

        /// <summary>
        /// Unregisters a command with the command handler
        /// </summary>
        /// <param name="command">the name of the command</param>
        /// <returns>True on success</returns>
        bool UnregisterCommand(string command);
    }
}
