﻿using System;
using System.Collections.Generic;

namespace LiteLoader.Game
{
    public interface IPlayerManager
    {
        /// <summary>
        /// Gets all recorded players
        /// </summary>
        /// <returns></returns>
        IEnumerable<IPlayer> All { get; }

        /// <summary>
        /// Gets all connected players
        /// </summary>
        /// <returns></returns>
        IEnumerable<IPlayer> Connected { get; }

        /// <summary>
        /// Ban a player with reason and duration
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        void Ban(string id, string reason = null, TimeSpan? duration = null);

        /// <summary>
        /// Find a player
        /// </summary>
        /// <param name="value">Player Object, Name, or ID</param>
        /// <param name="connectedOnly">Should only search for connected players</param>
        /// <returns></returns>
        IPlayer Find(object value, bool connectedOnly = false);

        /// <summary>
        /// Kick a player with a reason
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reason"></param>
        void Kick(string id, string reason = null);

        /// <summary>
        /// Unban a player
        /// </summary>
        /// <param name="id"></param>
        void Unban(string id);
    }
}
