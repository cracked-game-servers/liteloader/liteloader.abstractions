﻿namespace LiteLoader.Game
{
    public interface IPosition
    {
        /// <summary>
        /// Gets the current position of the referencing object
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void GetPosition(out double x, out double y, out double z);

        /// <summary>
        /// Sets the position of the referencing object
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void SetPosition(double? x, double? y, double? z);
    }
}
