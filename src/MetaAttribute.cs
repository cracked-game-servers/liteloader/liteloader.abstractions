﻿using System;

namespace LiteLoader
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public abstract class MetaAttribute : Attribute
    {
        /// <summary>
        /// A human readable Name for the object
        /// </summary>
        public string Title { get; }

        protected MetaAttribute(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title));
            }

            Title = title;
        }
    }
}
