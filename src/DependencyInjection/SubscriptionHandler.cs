﻿using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LiteLoader.DependencyInjection
{
    public abstract class SubscriptionHandler<TSubscription> : ISubscriptionHandler<TSubscription> where TSubscription : class
    {
        #region Helper Class

#pragma warning disable CA1063 // Implement IDisposable Correctly
        protected class UnsubscribeHelper : IDisposable
#pragma warning restore CA1063 // Implement IDisposable Correctly
        {
            private readonly SubscriptionHandler<TSubscription> mainContainer;
            private readonly TSubscription reference;
            private bool hasUnsubbed = false;

            public UnsubscribeHelper(TSubscription sub, SubscriptionHandler<TSubscription> container)
            {
                reference = sub;
                mainContainer = container;
            }

#pragma warning disable CA1063 // Implement IDisposable Correctly
            ~UnsubscribeHelper()
#pragma warning restore CA1063 // Implement IDisposable Correctly
            {
                Unsubscribe(false);
            }

#pragma warning disable CA1063 // Implement IDisposable Correctly
            public void Dispose() => Unsubscribe(true);
#pragma warning restore CA1063 // Implement IDisposable Correctly

            private void Unsubscribe(bool disposed)
            {
                if (hasUnsubbed)
                {
                    return;
                }

                hasUnsubbed = true;

                if (disposed)
                {
                    GC.SuppressFinalize(this);
                }

                mainContainer.Unsubscribe(reference);
            }
        }

        #endregion

        protected readonly List<TSubscription> Subscriptions;
        private readonly IInvokeManager _invoker;
        private readonly Type genericType;
        private readonly bool requireUnique;

        /// <summary>
        /// Creates a new instance of <see cref="SubscriptionHandler{TSubscription}"/>
        /// </summary>
        /// <param name="uniqueSubscriptions">Should only unique types be registered</param>
        protected SubscriptionHandler(bool uniqueSubscriptions = false, IEnumerable<TSubscription> staticSubscriptions = null)
        {
            _invoker = (this is IInvokeManager invoker) ? invoker : Interface.LiteLoader.ServiceProvider.GetService<IInvokeManager>();
            Subscriptions = new List<TSubscription>();
            genericType = typeof(TSubscription);
            requireUnique = uniqueSubscriptions;

            if (staticSubscriptions != null) Subscriptions.AddRange(staticSubscriptions);
        }

        /// <summary>
        /// Subscribes a object to this <see cref="SubscriptionHandler{TSubscription}"/>
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public void Subscribe(Type subscription, params object[] arguments)
        {
            if (subscription == null)
            {
                throw new ArgumentNullException(nameof(subscription));
            }

            if (subscription.IsAbstract || !genericType.IsAssignableFrom(subscription))
            {
                throw new ArgumentException("Invalid Subscription Type", nameof(subscription));
            }

            if (arguments == null) arguments = ArrayPool.Get<object>(0);

            TSubscription subscription1 = null;

            lock (Subscriptions)
            {
                if (requireUnique && Subscriptions.Any(sub => sub.GetType().FullName.Equals(subscription.FullName, StringComparison.Ordinal)))
                {
                    throw new InvalidOperationException("Subscription already exists");
                }

                var constructors = subscription.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (!_invoker.TryFindBestMatch(constructors, arguments, out MethodBase best, out object[] context, out int?[] map))
                {
                    throw new InvalidOperationException($"Failed to find a valid constructor for {subscription.FullName}");
                }

                try
                {
                    subscription1 = (TSubscription)_invoker.Invoke(best, context);
                    OnSubscribe(subscription1);
                    Subscriptions.Add(subscription1);
                }
                finally
                {
                    ArrayPool.Free(context);
                    ArrayPool.Free(map);
                }
            }

            Interface.LiteLoader.DisposeOnUnload(subscription.Assembly, new UnsubscribeHelper(subscription1, this));
        }

        /// <summary>
        /// Subscribes a object to this <see cref="SubscriptionHandler{TSubscription}"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public void Subscribe<T>(params object[] parameters) where T : TSubscription => Subscribe(typeof(T), parameters);

        /// <summary>
        /// Called after the subsciption object is created but before registered
        /// </summary>
        /// <remarks>Throw exception to prevent registration</remarks>
        /// <param name="subscription"></param>
        protected virtual void OnSubscribe(TSubscription subscription)
        {
        }

        /// <summary>
        /// Called after the subscription has been unregistered
        /// </summary>
        /// <param name="subscription"></param>
        protected virtual void OnUnsubscribed(TSubscription subscription)
        {
        }

        private void Unsubscribe(TSubscription subscription)
        {
            lock (Subscriptions)
            {
                Subscriptions.Remove(subscription);
            }

            if (subscription is IDisposable disposable)
            {
                disposable.Dispose();
            }

            OnUnsubscribed(subscription);
        }
    }
}
