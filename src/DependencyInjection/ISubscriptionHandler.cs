﻿using System;

namespace LiteLoader.DependencyInjection
{
    public interface ISubscriptionHandler
    {
        /// <summary>
        /// Subscribes a new subscription to this handler
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        void Subscribe(Type subscription, params object[] parameters);
    }

    public interface ISubscriptionHandler<TSubscription> : ISubscriptionHandler where TSubscription : class
    {
        /// <summary>
        /// Subscribes a new subscription to this handler
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters"></param>
        /// <returns></returns>
        void Subscribe<T>(params object[] parameters) where T : TSubscription;
    }
}
