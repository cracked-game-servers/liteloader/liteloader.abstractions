﻿using System;

namespace LiteLoader.DependencyInjection
{
    /// <summary>
    /// Responsible for all the services created within LiteLoader
    /// </summary>
    public interface IDynamicServiceCollection : IServiceProvider
    {
        /// <summary>
        /// Adds a service to this service provider
        /// </summary>
        /// <param name="descriptor"></param>
        void Add(ServiceDescriptor descriptor);

        /// <summary>
        /// Check if this provider contains a service
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        bool Contains(Type type);
    }
}
