﻿using System.Reflection;

namespace LiteLoader.DependencyInjection
{
    public interface IParameterBinding
    {
        /// <summary>
        /// Attempts to bind argument to given parameter
        /// </summary>
        /// <param name="parameter">The target parameter</param>
        /// <param name="inputValue">The provided value</param>
        /// <param name="outputValue">The output value to assign to the call context</param>
        /// <returns>True if bind was a success</returns>
        bool TryBind(ParameterInfo parameter, object inputValue, out object outputValue);
    }
}
