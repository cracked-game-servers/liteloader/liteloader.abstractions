﻿using System.Collections.Generic;
using System.Reflection;

namespace LiteLoader.DependencyInjection
{
    public interface IInvokeManager : ISubscriptionHandler<IParameterBinding>
    {
        /// <summary>
        /// Invoke a method or constructor and get the return value if any
        /// </summary>
        /// <param name="method">Method to invoke</param>
        /// <param name="orderedArguments">Method parameters</param>
        /// <param name="instance">Instance of the method if it is a instance member</param>
        /// <returns>The result of the executed method</returns>
        object Invoke(MethodBase method, object[] orderedArguments = null, object instance = null);

        /// <summary>
        /// Maps a method and sets it up for invoking
        /// </summary>
        /// <param name="parameters">Method or constructor to setup</param>
        /// <param name="givenArguments">Any provided arguments</param>
        /// <param name="orderedArguments">
        /// <see cref="Utilities.ArrayPool"/> instance loaded with ordered arguments
        /// </param>
        /// <param name="map">
        /// <see cref="Utilities.ArrayPool"/> instance with numbered positions to map given
        /// arguments to method parameters
        /// </param>
        /// <returns>Number of parameter matches</returns>
        int MapParameters(ParameterInfo[] parameters, object[] givenArguments, out object[] orderedArguments, out int?[] map);

        /// <summary>
        /// Processes ref and out parameters and returns them back to the given argument array
        /// </summary>
        /// <param name="parameters">Parameters to process</param>
        /// <param name="givenArguments">Array to return the values too</param>
        /// <param name="context">Ordered arguments that was used to invoke a method</param>
        /// <param name="map">The mapped positions</param>
        void ProcessReferences(ParameterInfo[] parameters, object[] givenArguments, object[] context, int?[] map);

        /// <summary>
        /// Finds the best <see cref="MethodBase"/> out of a collection
        /// </summary>
        /// <param name="methods">The methods to search</param>
        /// <param name="arguments">Arguments to match to the method first</param>
        /// <param name="best">The best match</param>
        /// <param name="context">The Invoke Context</param>
        /// <param name="map">The argument mapping</param>
        /// <returns></returns>
        bool TryFindBestMatch(IEnumerable<MethodBase> methods, object[] arguments, out MethodBase best, out object[] context, out int?[] map);
    }
}
