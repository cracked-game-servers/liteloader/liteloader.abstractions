﻿using System;

namespace LiteLoader.DependencyInjection
{
    public sealed class ServiceDescriptor
    {
        public object Instance { get; set; }
        public Type InstanceType { get; }
        public bool IsTransient { get; }
        public Type ServiceType { get; }

        private ServiceDescriptor(Type serviceType, Type instanceType, bool isTransient)
        {
            ServiceType = serviceType;
            InstanceType = instanceType;
            IsTransient = isTransient;
        }

        public static ServiceDescriptor CreateSingleton(Type referenceType, Type instanceType, object instance = null)
        {
            if (referenceType == null && instanceType == null && instance == null)
            {
                throw new ArgumentException("No valid arguments provided");
            }

            if (instanceType == null)
            {
                if (instance != null)
                {
                    instanceType = instance.GetType();
                }
                else
                {
                    if (referenceType.IsAbstract)
                    {
                        throw new ArgumentException("No valid instance type defined");
                    }

                    instanceType = referenceType;
                }
            }

            if (referenceType == null)
            {
                referenceType = instanceType;
            }

            if (!referenceType.IsAssignableFrom(instanceType) || (instance != null && !referenceType.IsInstanceOfType(instance)))
            {
                throw new ArgumentException($"{instanceType.FullName} is not assignable to {referenceType.FullName}");
            }

            return new ServiceDescriptor(referenceType, instanceType, false) { Instance = instance };
        }

        public static ServiceDescriptor CreateSingleton(Type instanceType, object instance = null) => CreateSingleton(null, instanceType, instance);

        public static ServiceDescriptor CreateSingleton<TReference, TInstance>(TInstance instance = default) => CreateSingleton(typeof(TReference), typeof(TInstance), instance);

        public static ServiceDescriptor CreateSingleton<TInstance>(TInstance instance = default) => CreateSingleton(null, typeof(TInstance), instance);

        public static ServiceDescriptor CreateTransient(Type referenceType, Type instanceType)
        {
            if (referenceType == null && instanceType == null)
            {
                throw new ArgumentException("No valid arguments provided");
            }

            if (instanceType == null)
            {
                if (referenceType.IsAbstract)
                {
                    throw new ArgumentException("No valid instance type defined");
                }

                instanceType = referenceType;
            }

            if (referenceType == null)
            {
                referenceType = instanceType;
            }

            return new ServiceDescriptor(referenceType, instanceType, true);
        }

        public static ServiceDescriptor CreateTransient(Type instanceType) => CreateTransient(null, instanceType);

        public static ServiceDescriptor CreateTransient<TReference, TInstance>() => CreateTransient(typeof(TReference), typeof(TInstance));

        public static ServiceDescriptor CreateTransient<TInstance>() => CreateTransient(null, typeof(TInstance));
    }
}
