﻿using System.IO;
using System.Text;
using System.Xml;

namespace LiteLoader.Http
{
    public static class HttpExtensions
    {
        /// <summary>
        /// Writes raw bytes to the request stream
        /// </summary>
        /// <param name="request"></param>
        /// <param name="data">Raw Data</param>
        /// <param name="contentType">Content-Type Header</param>
        /// <returns></returns>
        public static IWebRequest WriteToStream(this IWebRequest request, byte[] data, string contentType = "application/x-www-form-urlencoded")
        {
            if (data == null || data.Length == 0) return request;

            using (Stream stream = request.WriteToStream())
            {
                stream.Write(data, 0, data.Length);
            }
            
            if (!string.IsNullOrEmpty(contentType)) request.SetHeader("Content-Type", contentType);
            request.SetHeader("Content-MD5", Utilities.CryptographyHelper.ComputeMD5(data));
            
            return request;
        }

        /// <summary>
        /// Writes a string to the request stream
        /// </summary>
        /// <param name="request"></param>
        /// <param name="data">String to write</param>
        /// <param name="encoding">String Encoding</param>
        /// <param name="contentType">Content-Type</param>
        /// <returns></returns>
        public static IWebRequest WriteToStream(this IWebRequest request, string data, Encoding encoding = null, string contentType = "application/x-www-form-urlencoded")
        {
            if (string.IsNullOrEmpty(data)) return request;
            if (encoding == null) encoding = Encoding.UTF8;

            if (string.IsNullOrEmpty(contentType)) contentType = "application/x-www-form-urlencoded";
            contentType = $"; charset={encoding.WebName}";
            

            byte[] binary = encoding.GetBytes(data);
            return request.WriteToStream(binary, contentType);
        }

        /// <summary>
        /// Writes xml document to the request stream
        /// </summary>
        /// <param name="request"></param>
        /// <param name="document">Xml document</param>
        /// <param name="encoding">String Encoding</param>
        /// <param name="settings">Xml Writer Settings</param>
        /// <returns></returns>
        public static IWebRequest WriteToStream(this IWebRequest request, XmlDocument document, Encoding encoding = null, XmlWriterSettings settings = null)
        {
            if (document == null) return request;
            if (encoding == null) encoding = Encoding.UTF8;

            string contentType = $"application/xml; charset={encoding.WebName}";
            if (settings == null) settings = new XmlWriterSettings();
            settings.Encoding = encoding;
            settings.CloseOutput = false;
#if !NET35 && !NET40
            settings.Async = false;
#endif

            using (Stream stream = request.WriteToStream())
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                document.WriteTo(writer);
                stream.Seek(0, SeekOrigin.Begin);
                request.SetHeader("Content-Length", stream.Length);
                request.SetHeader("Content-MD5", Utilities.CryptographyHelper.ComputeMD5(stream));
                stream.Seek(0, SeekOrigin.End);
            }

            request.SetHeader("Content-Type", contentType);
            return request;
        }
    }
}
