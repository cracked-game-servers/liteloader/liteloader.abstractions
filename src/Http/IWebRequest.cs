﻿using System;
using System.IO;

namespace LiteLoader.Http
{
    public interface IWebRequest
    {
        /// <summary>
        /// The provider that created this request
        /// </summary>
        IWebRequestProvider Provider { get; }
    
        /// <summary>
        /// The destination address
        /// </summary>
        Uri Destination { get; }

        /// <summary>
        /// The request method
        /// </summary>
        RequestMethod Method { get; }

        /// <summary>
        /// Has this request completed
        /// </summary>
        bool IsComplete { get; }

        /// <summary>
        /// Does this request have a response
        /// </summary>
        bool HasResponse { get; }

        /// <summary>
        /// Does this request have a error
        /// </summary>
        bool HasError { get; }

        /// <summary>
        /// Status Code
        /// </summary>
        int StatusCode { get; }

        /// <summary>
        /// Status Message
        /// </summary>
        string StatusText { get; }

        /// <summary>
        /// Sets a request header
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        IWebRequest SetHeader(string key, object value);

        /// <summary>
        /// Writes data to the request stream
        /// </summary>
        /// <returns></returns>
        Stream WriteToStream();

        /// <summary>
        /// Enqueues this request and fires the callback when complete
        /// </summary>
        /// <param name="callback"></param>
        void EnqueueResponse(Action<bool, IWebResponse> callback);
    }
}
