﻿using LiteLoader.Plugins;
using System;

namespace LiteLoader.Http
{
    public interface IWebRequestProvider
    {
        /// <summary>
        /// Creates a request object
        /// </summary>
        /// <param name="address">Destination Address</param>
        /// <param name="timeout">Time to wait before canceling this request</param>
        /// <param name="source">Plugin source</param>
        /// <returns></returns>
        IWebRequest PrepareRequest(Uri address, RequestMethod method = RequestMethod.GET, TimeSpan timeout = default, Plugin source = null);
    }
}
