﻿namespace LiteLoader.Http
{
    public interface IWebResponse
    {
        /// <summary>
        /// The request responsible for this response
        /// </summary>
        IWebRequest Request { get; }

        /// <summary>
        /// The response code
        /// </summary>
        int ResponseCode { get; }

        /// <summary>
        /// Reads a response header
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string GetHeader(string key);

        /// <summary>
        /// Gets the response data
        /// </summary>
        /// <returns></returns>
        byte[] GetResponseData();
    }
}
