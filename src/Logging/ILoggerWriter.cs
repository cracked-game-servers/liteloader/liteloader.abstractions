﻿namespace LiteLoader.Logging
{
    public interface ILogWriter
    {
        /// <summary>
        /// Writes a message to this logger
        /// </summary>
        /// <param name="message">Message to write</param>
        void Write(LogMessage message);
    }
}
