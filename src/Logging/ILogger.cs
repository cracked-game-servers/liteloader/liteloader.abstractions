﻿using LiteLoader.DependencyInjection;

namespace LiteLoader.Logging
{
    public interface ILogger : ISubscriptionHandler<ILogWriter>
    {
        /// <summary>
        /// Logs a message to all subloggers
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        void LogMessage(object message, string prefix = null, LogMessage.Level priority = Logging.LogMessage.Level.Information);
    }
}
