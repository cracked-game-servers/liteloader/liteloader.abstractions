﻿using System;

namespace LiteLoader.Logging
{
    public struct LogMessage
    {
        public enum Level { Unknown = 0, Information = 1, Warning = 2, Error = 4, Stacktrace = 8, Debug = 16 }

        public string Message { get; }
        public string Prefix { get; }
        public Level Priority { get; }

        public DateTime Timestamp { get; }

        private LogMessage(string message, string prefix, Level priority, DateTime time)
        {
            Prefix = prefix;
            Message = message;
            Priority = priority;
            Timestamp = time;
        }

        public static LogMessage Create(object message, string prefix = null, Level priority = Level.Information)
        {
            if (message == null || (message is string str && string.IsNullOrEmpty(str)))
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (message is Exception)
            {
                priority = Level.Stacktrace;
            }

            string pre = string.Empty;

            switch (priority)
            {
                case Level.Information:
                    pre += "[INFO]";
                    break;

                case Level.Warning:
                    pre += "[WARN]";
                    break;

                case Level.Error:
                    pre += "[ERR]";
                    break;

                case Level.Debug:
                    pre += "[DEBUG]";
                    break;

                default:
                    break;
            }

            if (message is Exception e)
            {
                pre += $"[{e.GetType().Name}]";
                message = $"{e.Message}\n" +
                    $"{e.StackTrace}";
            }

            pre += "[LiteLoader]";

            if (!string.IsNullOrEmpty(prefix))
            {
                pre += $"[{prefix}]";
            }

            DateTime now = DateTime.Now;
            return new LogMessage(message.ToString(), pre, priority, now);
        }

        public override string ToString() => ToString(false);

        public string ToString(bool timestamp, bool date = false)
        {
            string message = string.Empty;

            if (timestamp || date)
            {
                message += '(';

                if (date)
                {
                    message += $"{Timestamp.Year}-{Timestamp.Month}-{Timestamp.Day}";
                }

                if (timestamp)
                {
                    if (date)
                    {
                        message += ' ';
                    }

                    message += $"{Timestamp.Hour}:{Timestamp.Minute}:{Timestamp.Second}";
                }

                message += ")";
            }

            message += $"{Prefix}: {Message}";
            return message;
        }
    }
}
