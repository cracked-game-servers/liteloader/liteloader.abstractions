﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace LiteLoader.Collections
{
    public sealed class ThreadSafeEnumerator<T> : IEnumerator<T>
    {
        private readonly IEnumerator<T> inner;
        private readonly object syncRoot;

        public T Current => inner.Current;

        object IEnumerator.Current => Current;

        public ThreadSafeEnumerator(IEnumerator<T> inner, object @lock)
        {
            this.inner = inner;
            syncRoot = @lock;
            Monitor.Enter(syncRoot);
        }

        public void Dispose() => Monitor.Exit(syncRoot);

        public bool MoveNext() => inner.MoveNext();

        public void Reset() => inner.Reset();
    }
}
