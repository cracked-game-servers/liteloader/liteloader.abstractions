﻿using System.Collections;
using System.Collections.Generic;

namespace LiteLoader.Collections
{
    public sealed class ThreadSafeList<T> : ICollection<T>
    {
        private readonly List<T> collection;

        public int Count
        {
            get
            {
                lock (collection)
                {
                    return collection.Count;
                }
            }
        }

        public bool IsReadOnly { get; } = false;

        public ThreadSafeList() => collection = new List<T>();

        public ThreadSafeList(int capacity) => collection = new List<T>(capacity);

        public ThreadSafeList(IEnumerable<T> collection) => this.collection = new List<T>(collection);

        public void Add(T item)
        {
            lock (collection)
            {
                collection.Add(item);
            }
        }

        public void Clear()
        {
            lock (collection)
            {
                collection.Clear();
            }
        }

        public bool Contains(T item)
        {
            lock (collection)
            {
                return collection.Contains(item);
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (collection)
            {
                collection.CopyTo(array, arrayIndex);
            }
        }

        public IEnumerator<T> GetEnumerator() => new ThreadSafeEnumerator<T>(collection.GetEnumerator(), collection);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public bool Remove(T item)
        {
            lock (collection)
            {
                return collection.Remove(item);
            }
        }
    }
}
