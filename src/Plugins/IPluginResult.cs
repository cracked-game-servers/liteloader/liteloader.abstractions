﻿using LiteLoader.Results;

namespace LiteLoader.Plugins
{
    public interface IPluginResult : IResult
    {
        /// <summary>
        /// Plugin instance if any
        /// </summary>
        Plugin Plugin { get; }

        /// <summary>
        /// The name of the plugin
        /// </summary>
        string PluginName { get; }
    }
}
