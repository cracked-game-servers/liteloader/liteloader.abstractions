﻿using LiteLoader.DependencyInjection;
using LiteLoader.Game;
using LiteLoader.Logging;
using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LiteLoader.Plugins
{
    public abstract class PluginLoader
    {
        #region Fields & Properties

        private static readonly Type PluginType = typeof(Plugin);
        private readonly IEngine Engine;
        private readonly IPluginHandler Handler;
        private readonly IInvokeManager Invoker;
        private readonly ILogger _logger;
        private readonly Dictionary<string, Plugin> loadedPlugins;
        private readonly List<string> loadingPlugins;
        protected virtual IEnumerable<Type> ValidPluginTypes { get; }

        #endregion

        protected PluginLoader()
        {
            Handler = Interface.LiteLoader.ServiceProvider.GetService<IPluginHandler>();
            Invoker = Interface.LiteLoader.ServiceProvider.GetService<IInvokeManager>();
            Engine = Interface.LiteLoader.ServiceProvider.GetService<IEngine>();
            _logger = Interface.LiteLoader.ServiceProvider.GetService<ILogger>();
            ValidPluginTypes = new[] { typeof(Plugin) };
            loadedPlugins = new Dictionary<string, Plugin>();
            loadingPlugins = new List<string>();
        }

        #region Plugin Loading

        /// <summary>
        /// Check if a plugin is loaded
        /// </summary>
        /// <param name="pluginName">Name of the plugin</param>
        /// <returns></returns>
        public bool IsPluginLoaded(string pluginName)
        {
            if (string.IsNullOrEmpty(pluginName))
            {
                throw new ArgumentNullException(nameof(pluginName));
            }

            lock (loadedPlugins)
            {
                return loadedPlugins.ContainsKey(pluginName);
            }
        }

        /// <summary>
        /// Checks if a plugin is currently loading
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public bool IsPluginLoading(string pluginName)
        {
            if (string.IsNullOrEmpty(pluginName))
            {
                throw new ArgumentNullException(nameof(pluginName));
            }

            lock (loadingPlugins)
            {
                return loadingPlugins.Contains(pluginName);
            }
        }

        /// <summary>
        /// Creates a new instance of a plugin
        /// </summary>
        /// <param name="pluginName">Name of the plugin</param>
        /// <returns></returns>
        protected virtual Plugin InstantiatePlugin(string pluginName)
        {
            Type[] types = GetType().Assembly.GetTypes();
            Type pluginType = typeof(Plugin);
            for (int i = 0; i < types.Length; i++)
            {
                Type type = types[i];
                if (type.IsAbstract || type.IsInterface || !pluginType.IsAssignableFrom(type)) continue;

                if (ValidPluginTypes != null && ValidPluginTypes.All(t => !t.IsAssignableFrom(type))) continue;

                if (!type.Name.Equals(pluginName, StringComparison.Ordinal)) continue;

                var constructors = type.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                if (!Invoker.TryFindBestMatch(constructors, ArrayPool.Get<object>(0), out MethodBase best, out object[] context, out int?[] map)) continue;

                try
                {
                    return (Plugin)Invoker.Invoke(best, context);
                }
                finally
                {
                    ArrayPool.Free(context);
                    ArrayPool.Free(map);
                }
            }

            return null;
        }

        /// <summary>
        /// Queues a plugin for loading returns a plugin if its already loaded
        /// </summary>
        /// <param name="pluginName">Name of the plugin</param>
        /// <returns></returns>
        public Plugin Load(string pluginName)
        {
            if (string.IsNullOrEmpty(pluginName))
            {
                throw new ArgumentNullException(pluginName);
            }
            lock (loadedPlugins)
            {
                if (loadedPlugins.TryGetValue(pluginName, out Plugin loadedPlugin))
                {
                    return loadedPlugin;
                }
            }
            lock (loadingPlugins)
            {
                if (loadingPlugins.Contains(pluginName))
                {
                    return null;
                }

                loadingPlugins.Add(pluginName);
            }

            Plugin plugin = null;

            try
            {
                plugin = InstantiatePlugin(pluginName);
                plugin.Name = pluginName;
                PluginAttribute attribute = plugin.GetType().GetCustomAttributes(false).OfType<PluginAttribute>().FirstOrDefault();
                if (attribute == null)
                {
                    Log($"Failed to load plugin {pluginName} bucause it is not decorated with PluginAttribute", LogMessage.Level.Error);
                    plugin = null;
                }

                if (string.IsNullOrEmpty(plugin.Title)) plugin.Title = string.IsNullOrEmpty(attribute.Title) ? plugin.Name : attribute.Title;
                if (string.IsNullOrEmpty(plugin.Author)) plugin.Author = attribute.Author;
                if (plugin.Version == null) plugin.Version = attribute.Version;
                if (string.IsNullOrEmpty(plugin.PluginDirectory)) plugin.PluginDirectory = Path.Combine(Interface.LiteLoader.PluginDirectory, pluginName);
                if (string.IsNullOrEmpty(plugin.DataDirectory)) plugin.DataDirectory = Path.Combine(plugin.PluginDirectory, "Data");

                if (!Directory.Exists(plugin.PluginDirectory)) Directory.CreateDirectory(plugin.PluginDirectory);
                if (!Directory.Exists(plugin.DataDirectory)) Directory.CreateDirectory(plugin.DataDirectory);

                plugin.BeginInit(Interface.LiteLoader, this);
                plugin.InvokeHook("Init", ArrayPool.Get<object>(0), true);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception e)
            {
                Log($"Failed to Initialize plugin {plugin?.Name ?? pluginName} v{plugin?.Version.ToString() ?? "0.0.0"} by {plugin?.Author ?? "Unknown Author"}", LogMessage.Level.Error);
                Log(e, LogMessage.Level.Stacktrace);
                if (plugin != null) plugin.OnHookStat -= OnHookStat;
                plugin = null;
            }
#pragma warning restore CA1031 // Do not catch general exception types

            if (plugin == null)
            {
                lock (loadingPlugins)
                {
                    loadingPlugins.Remove(pluginName);
                }

                return null;
            }

            Engine.NextUpdate(() => LoadPlugin(plugin));
            return null;
        }

        /// <summary>
        /// Finalizes plugin loading
        /// </summary>
        /// <param name="plugin">Plugin instance to finish loading</param>
        /// <param name="retries">Current attempts</param>
        protected void LoadPlugin(Plugin plugin, int retries = 0)
        {
            lock (loadingPlugins)
            {
                if (!loadingPlugins.Contains(plugin.Name))
                {
                    UnloadPlugin(plugin);
                    Log($"Unloaded plugin {plugin.Title} while still in loading state", LogMessage.Level.Warning);
                    return;
                }
            }

            try
            {
                lock (loadedPlugins)
                {
                    loadedPlugins[plugin.Name] = plugin;
                }

                plugin.InvokeHook("Loaded", ArrayPool.Get<object>(0));

                lock (loadingPlugins)
                {
                    loadingPlugins.Remove(plugin.Name);
                }

                OnPluginLoaded(plugin);
                Log($"Loaded plugin {plugin.Title} v{plugin.Version} by {plugin.Author}", LogMessage.Level.Information);
                Interface.Invoke("OnPluginLoaded", plugin);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception e)
            {
                Log($"Failed to load plugin {plugin.Title} v{plugin.Version} by {plugin.Author} (Attempt {retries} of {3})", LogMessage.Level.Error);
                Log(e, LogMessage.Level.Stacktrace);

                if (retries < 3)
                {
                    Engine.Delay(TimeSpan.FromSeconds(1), () => LoadPlugin(plugin, retries + 1));
                }
                else
                {
                    lock (loadingPlugins)
                    {
                        loadingPlugins.Remove(plugin.Name);
                    }
                }
            }
#pragma warning restore CA1031 // Do not catch general exception types
        }

        /// <summary>
        /// Called when a plugin has been successfully loaded
        /// </summary>
        /// <param name="plugin"></param>
        protected virtual void OnPluginLoaded(Plugin plugin)
        {
        }

        /// <summary>
        /// Called when the Core is ready to load all plugins
        /// </summary>
        public void LoadAll()
        {
            foreach (string plugin in ScanForPlugins())
            {
                Load(plugin);
            }
        }

        /// <summary>
        /// Reloads a plugin if loaded
        /// </summary>
        /// <param name="pluginName">Name of the plugin</param>
        /// <returns></returns>
        public bool Reload(string pluginName)
        {
            if (string.IsNullOrEmpty(pluginName)) return false;

            lock (loadedPlugins)
            {
                if (loadedPlugins.TryGetValue(pluginName, out Plugin plugin))
                {
                    UnloadPlugin(plugin);
                    Engine.NextUpdate(() => Load(pluginName));
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Plugin Unloading

        /// <summary>
        /// Unloads a plugin
        /// </summary>
        /// <param name="pluginName">Name of the plugin</param>
        public void Unload(string pluginName)
        {
            if (string.IsNullOrEmpty(pluginName))
            {
                return;
            }

            lock (loadingPlugins)
            {
                loadingPlugins.Remove(pluginName);
            }

            lock (loadedPlugins)
            {
                if (loadedPlugins.TryGetValue(pluginName, out Plugin plugin))
                {
                    UnloadPlugin(plugin);
                }
            }
        }

        /// <summary>
        /// Call to finalize plugin unloading
        /// </summary>
        /// <param name="plugin">Plugin instance to finish unloading</param>
        protected void UnloadPlugin(Plugin plugin)
        {
            lock (loadingPlugins)
            {
                loadingPlugins.Remove(plugin.Name);
            }

            lock (loadedPlugins)
            {
                loadedPlugins.Remove(plugin.Name);
            }

            plugin.InvokeHook("Unload", ArrayPool.Get<object>(0));
            plugin.BeginShutdown();
            plugin.OnHookStat -= OnHookStat;
            Log($"Unloaded plugin {plugin.Title} v{plugin.Version} by {plugin.Author}");
            Interface.Invoke("OnPluginUnloaded", plugin);
            OnPluginUnloaded(plugin);
            plugin = null;
            GC.Collect();
        }

        /// <summary>
        /// Called when a plugin has been unloaded
        /// </summary>
        /// <param name="plugin"></param>
        protected virtual void OnPluginUnloaded(Plugin plugin)
        {
        }

        /// <summary>
        /// Called when the Core wants to unload all loaded plugins by this loader
        /// </summary>
        public void UnloadAll()
        {
            string[] plugins = null;
            lock (loadedPlugins)
            {
                plugins = loadedPlugins.Keys.ToArray();
            }

            foreach (string str in plugins) Unload(str);
        }

        #endregion

        #region Plugin Helpers

        /// <summary>
        /// Returns a plugin if it's loaded
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public Plugin GetPlugin(string pluginName)
        {
            if (string.IsNullOrEmpty(pluginName))
            {
                throw new ArgumentNullException(nameof(pluginName));
            }

            lock (loadedPlugins)
            {
                if (loadedPlugins.TryGetValue(pluginName, out Plugin plugin))
                {
                    return plugin;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if this loader is responsible for a plugin
        /// </summary>
        /// <param name="pluginName">Name of the plugin</param>
        /// <returns></returns>
        public virtual bool HasPlugin(string pluginName) => ScanForPlugins().Contains(pluginName);

        /// <summary>
        /// Enumerates all plugins supported by this loader
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<string> ScanForPlugins()
        {
            Type[] types = GetType().Assembly.GetTypes();

            Type pluginType = typeof(Plugin);
            for (int i = 0; i < types.Length; i++)
            {
                Type type = types[i];
                if (type.IsAbstract || type.IsInterface || !pluginType.IsAssignableFrom(type)) continue;

                if (ValidPluginTypes != null && ValidPluginTypes.All(t => !t.IsAssignableFrom(type))) continue;

                yield return type.Name;
            }
        }

        public object InvokePlugins(string hookName, object[] arguments)
        {
            object[] responses = null;

            lock (loadedPlugins)
            {
                responses = ArrayPool.Get<object>(loadedPlugins.Count);

                int i = 0;
                foreach (var value in loadedPlugins.Values)
                {
                    responses[i] = value.InvokeHook(hookName, arguments, false);
                    i++;
                }
            }

            for (int i = 0; i < responses.Length; i++)
            {
                object value = responses[i];
                if (value == null || value == DBNull.Value)
                {
                    continue;
                }

                if (value != null)
                {
                    ArrayPool.Free(responses);
                    return value;
                }
            }
            ArrayPool.Free(responses);
            return null;
        }

        protected virtual void OnHookStat(Plugin plugin, string hookName, bool failed, int totalMilliseconds)
        {
            if (totalMilliseconds > 200)
            {
                Log($"Execution of {hookName} on plugin {plugin.Title} took {totalMilliseconds}ms", LogMessage.Level.Debug);
            }
        }

        #endregion

        #region Logging

        protected void Log(object message, LogMessage.Level level = LogMessage.Level.Information)
        {
            if (_logger == null) return;
            _logger.LogMessage(message, "PluginLoader", level);
        }

        #endregion
    }
}
