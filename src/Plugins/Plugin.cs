﻿using LiteLoader.Data;
using LiteLoader.Game;
using LiteLoader.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace LiteLoader.Plugins
{
    public delegate void PluginHookStatDelegate(Plugin plugin, string hookName, bool failed, int executionTime);

    public abstract class Plugin
    {
        #region Information

        /// <summary>
        /// Plugin Author
        /// </summary>
        public string Author { get; internal set; }

        /// <summary>
        /// This plugins data directory
        /// </summary>
        public string DataDirectory { get; internal set; }

        /// <summary>
        /// Has the plugin been loaded
        /// </summary>
        public bool IsLoaded { get; private set; } = false;

        /// <summary>
        /// Plugin Name
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// This plugins directory
        /// </summary>
        public string PluginDirectory { get; internal set; }

        /// <summary>
        /// Plugin Title
        /// </summary>
        public string Title { get; internal set; }

        /// <summary>
        /// Plugin Version
        /// </summary>
        public Version Version { get; internal set; }

        #endregion

        #region Resources

        /// <summary>
        /// The last exception thrown during hook execution
        /// </summary>
        public Exception LastException { get; private set; }

        /// <summary>
        /// Configuration file for this plugin
        /// </summary>
        protected IFileDataSource ConfigFile { get; private set; }

        /// <summary>
        /// Loader responsible for this plugin
        /// </summary>
        protected PluginLoader PluginLoader { get; private set; }

        #endregion

        #region Events

        /// <summary>
        /// Called after execution time for a hook has been calculated
        /// </summary>
        public event PluginHookStatDelegate OnHookStat;

        #endregion

        #region Services

        protected ICommandHandler Commands { get; private set; }
        protected IEngine GameEngine { get; private set; }
        protected ISaveProvider SaveHandler { get; private set; }
        private ILogger Logger { get; set; }

        #endregion

        #region Disposable Support

        private readonly HashSet<IDisposable> _registeredUnloadables = new HashSet<IDisposable>();

        /// <summary>
        /// Registers a item to be disposed when this plugin is unloaded
        /// </summary>
        /// <param name="disposable"></param>
        protected void DisposeOnUnload(IDisposable disposable)
        {
            if (!IsLoaded || disposable == null || _registeredUnloadables.Contains(disposable)) return;
            _registeredUnloadables.Add(disposable);
        }

        /// <summary>
        /// Removes a disposabled from being disposed when this plugin is unloaded
        /// </summary>
        /// <param name="disposable"></param>
        protected void UnregisterDisposable(IDisposable disposable)
        {
            if (disposable == null || !_registeredUnloadables.Contains(disposable)) return;
            _registeredUnloadables.Remove(disposable);
        }

        #endregion

        #region Initialization and Shutdown

        internal virtual void BeginInit(ILiteLoader liteloader, PluginLoader loader)
        {
            if (IsLoaded) return;
            if (liteloader != null && !AppDomain.CurrentDomain.IsDefaultAppDomain())
            {
                Interface.SetLoaderContext(liteloader);
            }

            PluginLoader = loader;
            ConfigFile = new IniDataFile(Path.Combine(PluginDirectory, $"{Name}.ini"), false);
            Logger = Interface.LiteLoader.ServiceProvider.GetService<ILogger>();
            GameEngine = Interface.LiteLoader.ServiceProvider.GetService<IEngine>();
            Commands = Interface.LiteLoader.ServiceProvider.GetService<ICommandHandler>();
            SaveHandler = Interface.LiteLoader.ServiceProvider.GetService<ISaveProvider>();
            IsLoaded = true;
        }

        internal virtual void BeginShutdown()
        {
            if (!IsLoaded) return;
            IsLoaded = false;
            foreach (IDisposable disposable in _registeredUnloadables)
            {
                try
                {
                    disposable.Dispose();
                }
                catch (Exception)
                {
                }
            }

            _registeredUnloadables.Clear();
            SaveHandler.SaveImmediately(ConfigFile);
            SaveHandler = null;
            ConfigFile = null;
            Commands = null;
            GameEngine = null;
            PluginLoader = null;
        }

        #endregion

        #region Hook Management

        /// <summary>
        /// Invokes a hook on this plugin
        /// </summary>
        /// <param name="name">Hook Name</param>
        /// <param name="arguments">Arguments to provide to the hook</param>
        /// <returns></returns>
        public object InvokeHook(string name, object[] arguments, bool throwException = false)
        {
            int startTime = Environment.TickCount;
            object value = DBNull.Value;
            bool failed = false;
            try
            {
                value = OnCallHook(name, arguments);
            }
            catch (Exception e)
            {
                value = DBNull.Value;

                if (e is TargetInvocationException && e.InnerException != null) e = e.InnerException;

                if (throwException)
                {
                    throw e;
                }
            }
            finally
            {
                try
                {
                    OnHookStat?.Invoke(this, name, failed, Environment.TickCount - startTime);
                }
                catch (Exception)
                {
                }
            }

            return value;
        }

        /// <summary>
        /// Used to intercept hook calls
        /// </summary>
        /// <param name="name"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        protected abstract object OnCallHook(string name, object[] arguments);

        #endregion

        #region Logging

        protected void Log(object message, LogMessage.Level level = LogMessage.Level.Information)
        {
            if (!IsLoaded) return;
            Logger.LogMessage(message, Title, level);
        }

        protected void LogDebug(object message) => Log(message, LogMessage.Level.Debug);

        protected void LogError(object message) => Log(message, LogMessage.Level.Error);

        protected void LogException(Exception e) => Log(e, LogMessage.Level.Stacktrace);

        protected void LogInfo(object message) => Log(message, LogMessage.Level.Information);

        protected void LogWarning(object message) => Log(message, LogMessage.Level.Warning);

        #endregion
    }
}
