﻿using System;
using System.Linq;

namespace LiteLoader.Plugins
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class CommandAttribute : Attribute
    {
        internal string[] Commands { get; }

        /// <summary>
        /// Attribute used to register plugin commands
        /// </summary>
        /// <param name="command">Command Name</param>
        public CommandAttribute(string command)
        {
            if (string.IsNullOrEmpty(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            Commands = new string[1] { command };
        }

        /// <summary>
        /// Attribute used to register plugin commands
        /// </summary>
        /// <param name="commands">Array of command names</param>
        public CommandAttribute(params string[] commands)
        {
            if (commands == null || commands.Length == 0)
            {
                throw new ArgumentNullException(nameof(commands));
            }

            Commands = commands.Where(s => !string.IsNullOrEmpty(s))
                .Distinct(StringComparer.OrdinalIgnoreCase)
                .ToArray();
        }
    }
}
