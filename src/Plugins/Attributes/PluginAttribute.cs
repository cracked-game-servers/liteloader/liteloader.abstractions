﻿using System;

namespace LiteLoader.Plugins
{
    /// <summary>
    /// Provides basic info for a <see cref="Plugin"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class PluginAttribute : Attribute
    {
        /// <summary>
        /// Plugin Author
        /// </summary>
        public string Author { get; }

        /// <summary>
        /// Plugin Title
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Plugin Version
        /// </summary>
        public Version Version { get; }

        public PluginAttribute(string title, string author, string version)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title));
            }

            Title = title;

            if (string.IsNullOrEmpty(author))
            {
                throw new ArgumentNullException(nameof(author));
            }

            Author = author;

            if (string.IsNullOrEmpty(version))
            {
                throw new ArgumentNullException(nameof(version));
            }

            Version = new Version(version);
        }

        public PluginAttribute(string title, string author, double version) : this(title, author, version.ToString())
        {
        }
    }
}
