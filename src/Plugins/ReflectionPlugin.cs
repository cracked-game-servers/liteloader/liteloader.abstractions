﻿using LiteLoader.DependencyInjection;
using LiteLoader.Game;
using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LiteLoader.Plugins
{
    public abstract class ReflectionPlugin : Plugin
    {
        #region Helper Classes

        [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
        protected sealed class HookAttribute : Attribute
        {
            /// <summary>
            /// Should ignore this hook
            /// </summary>
            public bool Ignore { get; set; } = false;

            /// <summary>
            /// Name of the Hook
            /// </summary>
            public string Name { get; set; }
        }

        #endregion

        #region Services

        private IInvokeManager Invoker { get; set; }

        #endregion

        #region Initialization and Shutdown

        internal override void BeginInit(ILiteLoader liteloader, PluginLoader loader)
        {
            base.BeginInit(liteloader, loader);
            Invoker = Interface.LiteLoader.ServiceProvider.GetService<IInvokeManager>();
            Subscribe("*");
            ProcessAllCommands(false);
        }

        internal override void BeginShutdown()
        {
            ProcessAllCommands(true);
            Unsubscribe("*");
            base.BeginShutdown();
        }

        #endregion

        #region Invoking

        private readonly Dictionary<string, List<KeyValuePair<MethodInfo, ParameterInfo[]>>> methodCache = new Dictionary<string, List<KeyValuePair<MethodInfo, ParameterInfo[]>>>();

        protected override object OnCallHook(string name, object[] arguments)
        {
            if (string.IsNullOrEmpty(name))
            {
                return DBNull.Value;
            }

            List<KeyValuePair<MethodInfo, ParameterInfo[]>> cache;

            lock (methodCache)
            {
                if (!methodCache.TryGetValue(name, out cache))
                {
                    return DBNull.Value;
                }
            }

            MethodInfo best = null;
            ParameterInfo[] parameters = null;
            object[] context = null;
            int?[] map = null;
            int total = -1;

            lock (cache)
            {
                for (int i = 0; i < cache.Count; i++)
                {
                    KeyValuePair<MethodInfo, ParameterInfo[]> pair = cache[i];
                    MethodInfo method = pair.Key;
                    var call = Invoker.MapParameters(pair.Value, arguments, out object[] c, out int?[] m);
                    if (call < 0)
                    {
                        continue;
                    }

                    if (best == null)
                    {
                        best = method;
                        parameters = pair.Value;
                        context = c;
                        map = m;
                        total = call;
                        continue;
                    }

                    if (call > total)
                    {
                        ArrayPool.Free(context);
                        ArrayPool.Free(map);
                        best = method;
                        context = c;
                        map = m;
                        parameters = pair.Value;
                        total = call;
                        continue;
                    }

                    ArrayPool.Free(c);
                    ArrayPool.Free(m);
                }
            }

            if (best == null)
            {
                return DBNull.Value;
            }

            try
            {
                object value = Invoker.Invoke(best, context, this);

                Invoker.ProcessReferences(parameters, arguments, context, map);
                return value == null && best.ReturnType == typeof(void) ? DBNull.Value : value;
            }
            finally
            {
                ArrayPool.Free(context);
                ArrayPool.Free(map);
            }
        }

        protected bool Subscribe(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            MethodInfo[] methods = GetType().GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            if (name.Equals("*", StringComparison.Ordinal))
            {
                return methods.Count(m => Subscribe(m)) > 0;
            }

            bool all = true;
            foreach (MethodInfo method in methods)
            {
                if (method.Name.Equals(name, StringComparison.Ordinal))
                {
                    bool value = Subscribe(method);
                    if (!value)
                    {
                        all = false;
                    }

                    continue;
                }

                if (!(method.GetCustomAttributes(typeof(HookAttribute), false) is HookAttribute[] hooks) || hooks.Length == 0)
                {
                    continue;
                }

                if (hooks.Any(a => a.Name.Equals(name, StringComparison.Ordinal)))
                {
                    bool value = Subscribe(method);

                    if (!value)
                    {
                        all = false;
                    }
                    continue;
                }
            }

            return all;
        }

        protected bool Unsubscribe(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            MethodInfo[] methods = GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance);

            if (name.Equals("*", StringComparison.Ordinal))
            {
                return methods.Count(m => Unsubscribe(m)) > 0;
            }

            bool all = true;
            foreach (MethodInfo method in methods)
            {
                if (method.Name.Equals(name, StringComparison.Ordinal))
                {
                    bool value = Unsubscribe(method);
                    if (!value)
                    {
                        all = false;
                    }

                    continue;
                }

                if (!(method.GetCustomAttributes(typeof(HookAttribute), false) is HookAttribute[] hooks) || hooks.Length == 0)
                {
                    continue;
                }

                if (hooks.Any(a => a.Name.Equals(name, StringComparison.Ordinal)))
                {
                    bool value = Unsubscribe(method);

                    if (!value)
                    {
                        all = false;
                    }
                    continue;
                }
            }

            return all;
        }

        private bool Subscribe(MethodInfo method)
        {
            if (method == null)
            {
                return false;
            }

            if (method.DeclaringType != GetType())
            {
                return false;
            }

            ParameterInfo[] parameters = method.GetParameters();
            HookAttribute[] attributes = method.GetCustomAttributes(typeof(HookAttribute), false) as HookAttribute[];

            if (attributes != null && attributes.Any(a => a.Ignore))
            {
                return false;
            }

            if (method.GetCustomAttributes(typeof(CommandAttribute), true) is CommandAttribute[] commands && commands.Length > 0)
            {
                return false;
            }

            bool added = false;

            lock (methodCache)
            {
                if (attributes != null && attributes.Length > 0)
                {
                    for (int i = 0; i < attributes.Length; i++)
                    {
                        HookAttribute c = attributes[i];
                        string name = string.IsNullOrEmpty(c.Name) ? method.Name : c.Name.Trim();

                        if (!methodCache.TryGetValue(name, out List<KeyValuePair<MethodInfo, ParameterInfo[]>> keys))
                        {
                            keys = new List<KeyValuePair<MethodInfo, ParameterInfo[]>>();
                            methodCache[name] = keys;
                        }

                        lock (keys)
                        {
                            if (keys.Any(kv => kv.Key == method))
                            {
                                continue;
                            }

                            keys.Add(new KeyValuePair<MethodInfo, ParameterInfo[]>(method, parameters));
                            added = true;
                        }
                    }
                }
                else
                {
                    if (!methodCache.TryGetValue(method.Name, out List<KeyValuePair<MethodInfo, ParameterInfo[]>> keys))
                    {
                        keys = new List<KeyValuePair<MethodInfo, ParameterInfo[]>>();
                        methodCache[method.Name] = keys;
                    }

                    lock (keys)
                    {
                        if (!keys.Any(kv => kv.Key == method))
                        {
                            keys.Add(new KeyValuePair<MethodInfo, ParameterInfo[]>(method, parameters));
                            added = true;
                        }
                    }
                }
            }
            return added;
        }

        private bool Unsubscribe(MethodInfo method)
        {
            if (method == null)
            {
                return false;
            }

            int removed = 0;

            lock (methodCache)
            {
                foreach (var cached in methodCache)
                {
                    lock (cached.Value)
                    {
                        removed += cached.Value.RemoveAll(kv => kv.Key == method);
                    }
                }
            }

            return removed > 0;
        }

        #endregion

        #region Command Handling

        protected void RegisterCommandMethod(MethodInfo method, string[] commands)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            if (commands == null)
            {
                throw new ArgumentNullException(nameof(commands));
            }

            commands = commands.Where(c => !string.IsNullOrEmpty(c))
                .Distinct()
                .ToArray();

            if (commands.Length == 0)
            {
                throw new ArgumentException("No valid command strings", nameof(commands));
            }

            ParameterInfo[] parameters = method.GetParameters();

            if (method.ReturnType != typeof(void) || parameters.Length != 1 || parameters[0].ParameterType != typeof(ICommandContext))
            {
                throw new ArgumentException("Method does not match the CommandCallback Pattern. Method must have a ReturnType of Void and a Single Parameter of LiteLoader.Game.ICommandContext");
            }

            CommandCallback callback = (CommandCallback)Delegate.CreateDelegate(typeof(CommandCallback), this, method, true);

            for (int i = 0; i < commands.Length; i++)
            {
                string command = commands[i];

                if (!Commands.RegisterCommand(command, callback, this))
                {
                    LogWarning($"Failed to register command {command} for method {method.Name}(LiteLoader.Game.ICommandContext {parameters[0].Name})");
                }
            }
        }

        private void ProcessAllCommands(bool unloading)
        {
            MethodInfo[] methods = GetType().GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            for (int i = 0; i < methods.Length; i++)
            {
                MethodInfo method = methods[i];

                if (!(method.GetCustomAttributes(typeof(CommandAttribute), true) is CommandAttribute[] commands) || commands.Length == 0) continue;

                string[] cmdStrs = commands.SelectMany(c => c.Commands).ToArray();

                if (!unloading)
                {
                    RegisterCommandMethod(method, cmdStrs);
                }
                else
                {
                    UnregisterCommandMethod(commands);
                }
            }
        }

        private void UnregisterCommandMethod(CommandAttribute[] attributes)
        {
            for (int i = 0; i < attributes.Length; i++)
            {
                CommandAttribute curr = attributes[i];

                for (int c = 0; c < curr.Commands.Length; c++)
                {
                    string cmd = curr.Commands[c];
                    Commands.UnregisterCommand(cmd);
                }
            }
        }

        #endregion
    }
}
