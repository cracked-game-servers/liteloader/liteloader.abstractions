﻿using System.Collections.Generic;

namespace LiteLoader.Plugins
{
    /// <summary>
    /// Responsible for all plugins
    /// </summary>
    public interface IPluginHandler
    {
        /// <summary>
        /// Get instance of a plugin by name
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        Plugin GetPlugin(string plugin);

        /// <summary>
        /// A list of plugin names
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetPlugins();

        /// <summary>
        /// Loads a Plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        IPluginResult LoadPlugin(string plugin);

        /// <summary>
        /// Reloads a Plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        IPluginResult ReloadPlugin(string plugin);

        /// <summary>
        /// Unloads a Plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        IPluginResult UnloadPlugin(string plugin);
    }
}
