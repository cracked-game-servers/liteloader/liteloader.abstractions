﻿using LiteLoader.Utilities;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LiteLoader
{
    public delegate void DebugCallback(Logging.LogMessage message);

    public static class Interface
    {
        #region Startup & Shutdown

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static ILiteLoader LiteLoader { get; private set; }

        [Obfuscation(StripAfterObfuscation = true, Feature = "anti debug")]
        public static void Initialize(string game, DebugCallback debugCallback = null)
        {
            if (LiteLoader != null)
            {
                throw new InvalidOperationException();
            }

            Assembly core = AppDomain.CurrentDomain.GetAssemblies()
                .FirstOrDefault(a => a.GetName().Name.Equals("LiteLoader.Core"));

            if (core == null)
            {
                FileInfo abs = new FileInfo(typeof(Interface).Assembly.Location);
                core = Assembly.LoadFrom(Path.Combine(abs.Directory.FullName, "LiteLoader.Core.dll"));
            }

            Type type = core.GetTypes()
                .FirstOrDefault(t => typeof(ILiteLoader).IsAssignableFrom(t) && !t.IsAbstract);
            LiteLoader = (ILiteLoader)Activator.CreateInstance(type, true);
            MethodInfo method = type.GetMethod("Load", BindingFlags.NonPublic | BindingFlags.Instance);

            method.Invoke(LiteLoader, new object[2] { game, debugCallback });
        }

        [Obfuscation(StripAfterObfuscation = true, Feature = "anti debug")]
        public static void Shutdown()
        {
            if (LiteLoader == null)
            {
                throw new InvalidOperationException();
            }

            Type type = LiteLoader.GetType();
            MethodInfo method = type.GetMethod("Unload", BindingFlags.NonPublic | BindingFlags.Instance);
            method.Invoke(LiteLoader, ArrayPool.Get<object>(0));
            LiteLoader = null;
        }

        [Obfuscation(StripAfterObfuscation = true, Feature = "anti debug")]
        internal static void SetLoaderContext(ILiteLoader liteLoader)
        {
            if (AppDomain.CurrentDomain.IsDefaultAppDomain())
            {
                throw new InvalidOperationException("This function can only be called by remote AppDomains");
            }

            if (LiteLoader == null)
            {
                LiteLoader = liteLoader;
            }
        }

        #endregion

        #region Invoking

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object[] arguments) => LiteLoader?.InvokeHook(name, arguments);

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name)
        {
            object[] context = ArrayPool.Get<object>(0);
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0)
        {
            object[] context = ArrayPool.Get<object>(1);
            context[0] = a0;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1)
        {
            object[] context = ArrayPool.Get<object>(2);
            context[0] = a0;
            context[1] = a1;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2)
        {
            object[] context = ArrayPool.Get<object>(3);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3)
        {
            object[] context = ArrayPool.Get<object>(4);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3, object a4)
        {
            object[] context = ArrayPool.Get<object>(5);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            context[4] = a4;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3, object a4, object a5)
        {
            object[] context = ArrayPool.Get<object>(6);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            context[4] = a4;
            context[5] = a5;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3, object a4, object a5, object a6)
        {
            object[] context = ArrayPool.Get<object>(7);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            context[4] = a4;
            context[5] = a5;
            context[6] = a6;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3, object a4, object a5, object a6, object a7)
        {
            object[] context = ArrayPool.Get<object>(8);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            context[4] = a4;
            context[5] = a5;
            context[6] = a6;
            context[7] = a7;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3, object a4, object a5, object a6, object a7, object a8)
        {
            object[] context = ArrayPool.Get<object>(9);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            context[4] = a4;
            context[5] = a5;
            context[6] = a6;
            context[7] = a7;
            context[8] = a8;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        [Obfuscation(Exclude = true, StripAfterObfuscation = true)]
        public static object Invoke(string name, object a0, object a1, object a2, object a3, object a4, object a5, object a6, object a7, object a8, object a9)
        {
            object[] context = ArrayPool.Get<object>(10);
            context[0] = a0;
            context[1] = a1;
            context[2] = a2;
            context[3] = a3;
            context[4] = a4;
            context[5] = a5;
            context[6] = a6;
            context[7] = a7;
            context[8] = a8;
            context[9] = a9;
            object value = Invoke(name, context);
            ArrayPool.Free(context);
            return value;
        }

        #endregion
    }
}
