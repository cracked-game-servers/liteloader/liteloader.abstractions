﻿using System;

namespace LiteLoader.Results
{
    /// <summary>
    /// A interface used to report a result of a task
    /// </summary>
    public interface IResult
    {
        /// <summary>
        /// Exception thrown if any
        /// </summary>
        Exception Exception { get; }

        /// <summary>
        /// A message provided with the result
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Indicates if the result is successful
        /// </summary>
        bool Success { get; }
    }
}
