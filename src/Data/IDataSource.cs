﻿using System.Collections.Generic;

namespace LiteLoader.Data
{
    /// <summary>
    /// Source of data
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// Retreive all available keys
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetKeys();

        /// <summary>
        /// Gets a value assigned to a given key
        /// </summary>
        /// <param name="key">The Key</param>
        /// <returns>The Value</returns>
        object GetValue(string key);

        /// <summary>
        /// Assigns a value to a key
        /// </summary>
        /// <param name="key">The Key</param>
        /// <param name="value">The Value</param>
        void SetValue(string key, object value = null);
    }
}
