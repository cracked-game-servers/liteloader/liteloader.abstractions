﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LiteLoader.Data.Patterns
{
    public abstract class VirtualPattern : Pattern
    {
        #region Helpers

        private delegate object VirtualGetMethod();

        private delegate void VirtualSetMethod(object value);

        private struct ActiveRecordMethodSet
        {
            public VirtualGetMethod Get { get; set; }
            public VirtualSetMethod Set { get; set; }
        }

        #endregion

        private Dictionary<string, ActiveRecordMethodSet> MethodCache { get; }

        protected VirtualPattern(IDataSource source) : base(source)
        {
            Type[] ignoreTypes = { typeof(Pattern), typeof(VirtualPattern) };
            MethodCache = new Dictionary<string, ActiveRecordMethodSet>(StringComparer.InvariantCultureIgnoreCase);

            var methods = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
            for (int i = 0; i != methods.Length; i++)
            {
                MethodInfo m = methods[i];
                if (ignoreTypes.Contains(m.DeclaringType)) continue;

                ParameterInfo[] p = m.GetParameters();

                bool isSet;

                if (m.ReturnType == typeof(void) && p.Length == 1 && p[0].ParameterType == typeof(object))
                {
                    isSet = true;
                }
                else if (m.ReturnType == typeof(object) && p.Length == 0)
                {
                    isSet = false;
                }
                else
                {
                    continue;
                }

                if (!MethodCache.TryGetValue(m.Name, out ActiveRecordMethodSet set))
                {
                    set = new ActiveRecordMethodSet();
                }

                if (isSet && set.Set != null)
                {
                    throw new InvalidOperationException($"Multiple 'Set' functions found for key '{m.Name}'");
                }

                if (!isSet && set.Get != null)
                {
                    throw new InvalidOperationException($"Multiple 'Get' functions found for key '{m.Name}'");
                }

                if (isSet) set.Set = (VirtualSetMethod)Delegate.CreateDelegate(typeof(VirtualSetMethod), this, m, true);
                else set.Get = (VirtualGetMethod)Delegate.CreateDelegate(typeof(VirtualGetMethod), this, m, true);

                MethodCache[m.Name] = set;
            }
        }

        public override object Get(string key)
        {
            if (!string.IsNullOrEmpty(key) && MethodCache.TryGetValue(key, out var set) && set.Get != null)
            {
                return set.Get();
            }

            return base.Get(key);
        }

        public override void Set(string key, object value)
        {
            if (!string.IsNullOrEmpty(key) && MethodCache.TryGetValue(key, out var set) && set.Set != null)
            {
                set.Set(value);
                return;
            }

            base.Set(key, value);
        }
    }
}
