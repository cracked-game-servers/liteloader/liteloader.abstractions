﻿using System;

namespace LiteLoader.Data.Patterns
{
    public interface IDataPattern : IDisposable
    {
        /// <summary>
        /// Gets or sets a key in the <see cref="IDataSource"/>
        /// </summary>
        /// <param name="key">The Key</param>
        /// <returns>The Value</returns>
        object this[string key] { get; set; }

        /// <summary>
        /// Gets a value assigned to a key
        /// </summary>
        /// <param name="key">The Key</param>
        /// <returns>The Value</returns>
        object Get(string key);

        /// <summary>
        /// Assigns a key in the <see cref="IDataSource"/>
        /// </summary>
        /// <param name="key">The Key</param>
        /// <param name="value">The Value</param>
        void Set(string key, object value);
    }
}
