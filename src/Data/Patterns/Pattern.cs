﻿using System;

namespace LiteLoader.Data.Patterns
{
#pragma warning disable CA1063 // Implement IDisposable Correctly
    public abstract class Pattern : IDataPattern
#pragma warning restore CA1063 // Implement IDisposable Correctly
    {
        protected IDataSource Data { get; }

        protected Pattern(IDataSource source)
        {
            Data = source ?? throw new ArgumentNullException(nameof(source));
        }

        public object this[string key] { get => Get(key); set => Set(key, value); }

#pragma warning disable CA1063 // Implement IDisposable Correctly
        public virtual void Dispose()
#pragma warning restore CA1063 // Implement IDisposable Correctly
        {
            if (Data is ISavable savable)
            {
                Interface.LiteLoader.ServiceProvider.GetService<ISaveProvider>().RegisterForSave(savable);
            }
        }

        public virtual object Get(string key) => Data.GetValue(key);

        public virtual void Set(string key, object value) => Data.SetValue(key, value);
    }
}
