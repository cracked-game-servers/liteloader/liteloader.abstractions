﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace LiteLoader.Data
{
    public class IniDataFile : IFileDataSource
    {
        protected readonly bool CaseSensitive;

        protected readonly List<Section> Sections;

        private static readonly Regex _keyValueMatch = new Regex(@"^(?'Key'[a-zA-Z0-9 ]+)=(?'Value'[a-zA-Z0-9 ]+)$", RegexOptions.Compiled);

        private static readonly Regex _sectionMatch = new Regex(@"^\[(?'Section'[a-zA-Z0-9- ]+)\]$", RegexOptions.Compiled);

        public string FilePath { get; }

        public bool HasChanges { get; protected set; }

        public IniDataFile(string fileName, bool caseSensitive = true)
        {
            CaseSensitive = caseSensitive;
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException(nameof(fileName));
            if (!fileName.EndsWith(".ini", StringComparison.InvariantCultureIgnoreCase))
            {
                fileName += ".ini";
            }

            var file = new FileInfo(fileName);
            FilePath = file.FullName;
            Sections = new List<Section>();
            if (file.Exists)
            {
                Load();
            }
        }

        public IEnumerable<string> GetKeys()
        {
            foreach (var section in Sections)
            {
                if (section.Data.Count == 0) continue;

                string sectionName = section.Name.Replace('-', ':');

                foreach (var k in section.Data.Keys)
                {
                    yield return sectionName + ':' + k;
                }
            }
        }

        public object GetValue(string key)
        {
            if (string.IsNullOrEmpty(key)) return null;

            string section = FindSection(ref key);
            Section sec = FindSection(section);
            if (sec == null) return null;

            if (sec.Data.TryGetValue(key, out string value)) return value;
            return null;
        }

        public void Load()
        {
            if (!File.Exists(FilePath))
            {
                throw new IOException($"{FilePath} does not exist");
            }

            string[] lines = File.ReadAllLines(FilePath, Encoding.ASCII);

            Section currentSection = null;

            for (int i = 0; i != lines.Length; i++)
            {
                string line = lines[i];

                if (string.IsNullOrEmpty(line)) continue;

                Match match = _sectionMatch.Match(line);

                if (match.Success)
                {
                    string sectionName = match.Groups["Section"].Value;
                    currentSection = FindOrCreateSection(sectionName);
                    continue;
                }

                if (currentSection == null)
                {
                    throw new FormatException($"Invalid Data - FilePath: {FilePath} | LineNumber: {i} | ExpectedToken: SECTION_HEADER | Value: {line}");
                }

                match = _keyValueMatch.Match(line);

                if (!match.Success) throw new FormatException($"Invalid Data - FilePath: {FilePath} | LineNumber: {i} | ExpectedToken: KEYVALUE_PAIR | Value: {line}");
                string key = match.Groups["Key"].Value;
                string value = match.Groups["Value"].Value;
                currentSection.Data[key] = value;
            }
        }

        public IEnumerator DoSave()
        {
            if (!HasChanges) yield break;
            
            HasChanges = false;

            FileStream file;

            try
            {
                if (!File.Exists(FilePath))
                {
#pragma warning disable IDE0068 // Use recommended dispose pattern
                    file = File.Create(FilePath);
#pragma warning restore IDE0068 // Use recommended dispose pattern
                }
                else
                {
#pragma warning disable IDE0068 // Use recommended dispose pattern
                    file = File.Open(FilePath, FileMode.Truncate);
#pragma warning restore IDE0068 // Use recommended dispose pattern
                }
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (IOException)
            {
                // TODO: Log?
                HasChanges = true;
                yield break;
            }
#pragma warning restore CA1031 // Do not catch general exception types

            yield return null;

            int lastReturnTime = Environment.TickCount;

            StringBuilder builder = new StringBuilder();

            foreach (Section section in Sections)
            {
                if (section.Data.Count > 0)
                {
                    builder.AppendLine($"[{section.Name}]");

                    foreach (KeyValuePair<string, string> kv in section.Data)
                    {
                        builder.AppendLine($"{kv.Key}={kv.Value}");
                    }
                    builder.AppendLine();
                }
            }

            byte[] writableData = Encoding.ASCII.GetBytes(builder.ToString());

            if (Environment.TickCount - lastReturnTime >= 1000)
            {
                yield return null;
                lastReturnTime = Environment.TickCount;
            }

            int lastStartIndex = 0;
            while (true)
            {
                int left = writableData.Length - lastStartIndex;
                if (left == 0) break;
                int count = 256;

                if (count > left) count = left;

                file.Write(writableData, lastStartIndex, count);
                lastStartIndex += count;

                if (Environment.TickCount - lastReturnTime >= 1000)
                {
                    yield return null;
                    lastReturnTime = Environment.TickCount;
                }
            }

            file.Dispose();
        }

        public void Save(bool immediately = false)
        {
            var save = Interface.LiteLoader.ServiceProvider.GetService<ISaveProvider>();

            if (immediately)
            {
                save.SaveImmediately(this);
            }
            else
            {
                save.RegisterForSave(this);
            }
        }

        public void SetValue(string key, object value = null)
        {
            if (string.IsNullOrEmpty(key)) throw new ArgumentNullException(nameof(key));

            string section = FindSection(ref key);
            Section sec = FindOrCreateSection(section);
            TypeConverter converter;
            if (sec.Data.TryGetValue(key, out string val))
            {
                if (value == null)
                {
                    sec.Data.Remove(key);
                    HasChanges = true;
                    return;
                }

                converter = TypeDescriptor.GetConverter(value);
                if (!converter.CanConvertTo(typeof(string)))
                {
                    throw new ArgumentException($"Failed to convert {value.GetType().FullName} to string");
                }
                string valueStringExisting = converter.ConvertToString(value);

                if (!val.Equals(valueStringExisting, StringComparison.InvariantCulture))
                {
                    sec.Data[key] = valueStringExisting;
                    HasChanges = true;
                }
                return;
            }

            if (value == null) return;

            converter = TypeDescriptor.GetConverter(value);
            if (!converter.CanConvertTo(typeof(string)))
            {
                throw new ArgumentException($"Failed to convert {value.GetType().FullName} to string");
            }

            string valueString = converter.ConvertToString(value);
            sec.Data[key] = valueString;
            HasChanges = true;
        }

        private Section FindOrCreateSection(string sectionName)
        {
            var section = FindSection(sectionName);

            if (section == null)
            {
                section = new Section(sectionName, new Dictionary<string, string>(CaseSensitive ? StringComparer.InvariantCulture : StringComparer.InvariantCultureIgnoreCase));
                Sections.Add(section);
            }

            return section;
        }

        private string FindSection(ref string key)
        {
            string section = "General";
            if (key.Contains(":"))
            {
                int lastIndex = key.LastIndexOf(':');
                section = key.Substring(0, lastIndex).Replace(':', '-');
                key = key.Substring(lastIndex + 1);
            }

            return !string.IsNullOrEmpty(section) ? section : throw new ArgumentNullException("Section");
        }

        private Section FindSection(string sectionName) => Sections.Find(s => s.Name.Equals(sectionName, CaseSensitive ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase));

        protected sealed class Section
        {
            public Dictionary<string, string> Data { get; }
            public string Name { get; set; }

            public Section(string name, Dictionary<string, string> values)
            {
                Name = !string.IsNullOrEmpty(name) ? name : throw new ArgumentNullException(nameof(name));
                Data = values ?? throw new ArgumentNullException(nameof(values));
            }
        }
    }
}
