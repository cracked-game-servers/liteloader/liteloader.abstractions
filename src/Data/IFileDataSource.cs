﻿namespace LiteLoader.Data
{
    /// <summary>
    /// A datasource that saves to a file
    /// </summary>
    public interface IFileDataSource : IDataSource, ISavable
    {
        /// <summary>
        /// The full path of the file
        /// </summary>
        string FilePath { get; }
    }
}
