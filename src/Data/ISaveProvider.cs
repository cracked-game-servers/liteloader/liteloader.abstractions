﻿namespace LiteLoader.Data
{
    /// <summary>
    /// Used to register <see cref="ISavable"/> objects
    /// </summary>
    public interface ISaveProvider
    {
        /// <summary>
        /// Processes <see cref="ISavable"/> Immediately
        /// </summary>
        /// <param name="savable"></param>
        void SaveImmediately(ISavable savable);

        /// <summary>
        /// Registers a <see cref="ISavable"/> to be saved and the next save
        /// </summary>
        /// <param name="savable"></param>
        void RegisterForSave(ISavable savable);
    }
}
