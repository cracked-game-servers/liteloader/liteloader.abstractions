﻿using System.Collections;

namespace LiteLoader.Data
{
    /// <summary>
    /// Interface used to process saveable data
    /// </summary>
    public interface ISavable
    {
        /// <summary>
        /// Called by the <see cref="ISaveProvider"/> when this object is being saved
        /// </summary>
        /// <returns></returns>
        IEnumerator DoSave();
    }
}
