﻿using System;
using System.Reflection;

namespace LiteLoader
{
    public interface ILiteLoader
    {
        #region Directories

        /// <summary>
        /// Directory containing all the extensions
        /// </summary>
        string ExtensionDirectory { get; }

        /// <summary>
        /// Directory containing plugins and plugin data
        /// </summary>
        string PluginDirectory { get; }

        /// <summary>
        /// Directory containing all the games dll's
        /// </summary>
        string LibraryDirectory { get; }

        /// <summary>
        /// Root Directory + LiteLoader
        /// </summary>
        string LoaderDirectory { get; }

        /// <summary>
        /// Directory containing logs
        /// </summary>
        string LoggingDirectory { get; }

        /// <summary>
        /// Directory usually containing the executable
        /// </summary>
        string RootDirectory { get; }

        /// <summary>
        /// Returns the directory of a registered extension
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        string GetExtensionPath(Assembly module);

        #endregion

        #region Services

        /// <summary>
        /// Contains all LiteLoaders services
        /// </summary>
        IServiceProvider ServiceProvider { get; }

        /// <summary>
        /// Registers a object to be disposed on module unload
        /// </summary>
        /// <param name="disposable"></param>
        /// <returns></returns>
        bool DisposeOnUnload(IDisposable disposable);

        /// <summary>
        /// Registers a object to be disposed on module unload
        /// </summary>
        /// <param name="module"></param>
        /// <param name="disposable"></param>
        /// <returns></returns>
        bool DisposeOnUnload(Assembly module, IDisposable disposable);

        #endregion

        #region Hooks

        /// <summary>
        /// Invokes a hook
        /// </summary>
        /// <param name="name">Hook Name</param>
        /// <param name="argument">Provided Parameter Values</param>
        /// <returns></returns>
        /// C:\Users\austi\source\repos\LiteLoader\LiteLoader.Abstractions\ILiteLoader.cs
        object InvokeHook(string name, object[] argument);

        #endregion
    }
}
