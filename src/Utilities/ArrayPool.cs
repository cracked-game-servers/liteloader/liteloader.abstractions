﻿using System;
using System.Collections.Generic;

namespace LiteLoader.Utilities
{
    public static class ArrayPool
    {
        public const int INITIAL_POOL_LENGTH = 10;
        public const int MAX_ARRAY_LENGTH = 50;
        public const int MAX_POOL_SIZE = 128;

        private static readonly Dictionary<Type, ArrayPoolInstance> _arrayPools = new Dictionary<Type, ArrayPoolInstance>
        {
            [typeof(object)] = new ArrayPoolInstance(typeof(object)),
            [typeof(int?[])] = new ArrayPoolInstance(typeof(int?[]))
        };

        private static readonly Type _defaultType = typeof(object);

        /// <summary>
        /// Returns the array back to the pool and empties the array
        /// </summary>
        /// <param name="array">The array to return back to the pool</param>
        public static void Free(Array array)
        {
            if (array == null || array.Length == 0) return;

            Type element = array.GetType().GetElementType();

            ArrayPoolInstance instance = null;
            lock (_arrayPools)
            {
                if (!_arrayPools.TryGetValue(element, out instance))
                {
                    return;
                }
            }

            instance.FreeArray(array);
        }

        /// <summary>
        /// Returns the array back to the pool and empties the array
        /// </summary>
        /// <param name="arrays">The arrays to return back to the pool</param>
        public static void Free(params Array[] arrays)
        {
            if (arrays == null || arrays.Length == 0) return;

            for (int i = 0; i < arrays.Length; i++)
            {
                Free(arrays[i]);
            }
        }

        /// <summary>
        /// Gets a pooled array
        /// </summary>
        /// <remarks>
        /// ArrayPool adhears to <see cref="MAX_ARRAY_LENGTH"/> and won't pool above that number
        /// </remarks>
        /// <param name="length">Requested length of the array</param>
        /// <param name="arrayType">Requested Array Type (Default = <see cref="System.Object"/>)</param>
        /// <returns></returns>
        public static Array Get(int length, Type arrayType = null)
        {
            if (arrayType == null) arrayType = _defaultType;

            if (length < 0) length = 0;

            ArrayPoolInstance instance = null;

            lock (_arrayPools)
            {
                if (!_arrayPools.TryGetValue(arrayType, out instance))
                {
                    instance = new ArrayPoolInstance(arrayType);
                    _arrayPools[arrayType] = instance;
                }
            }

            return instance.GetArray(length);
        }

        /// <summary>
        /// Gets a pooled array
        /// </summary>
        /// <remarks>
        /// ArrayPool adhears to <see cref="MAX_ARRAY_LENGTH"/> and won't pool above that number
        /// </remarks>
        /// <typeparam name="T">Requested Array Type</typeparam>
        /// <param name="length">Requested length of the array</param>
        /// <returns></returns>
        public static T[] Get<T>(int length) => (T[])Get(length, typeof(T));
    }
}
