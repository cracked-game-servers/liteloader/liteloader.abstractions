﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace LiteLoader.Utilities
{
    public static class CryptographyHelper
    {
        public static string ComputeMD5(Stream stream)
        {
            if (stream == null) return null;

            using (MD5 md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", string.Empty).ToLowerInvariant();
            }
        }

        public static string ComputeMD5(byte[] buffer)
        {
            if (buffer == null || buffer.Length == 0)
            {
                return null;
            }

            using (MD5 md5 = MD5.Create())
            {
                using (MemoryStream reader = new MemoryStream(buffer, false))
                {
                    var hash = md5.ComputeHash(reader);
                    return BitConverter.ToString(hash).Replace("-", string.Empty).ToLowerInvariant();
                }
            }
        }
    }
}
