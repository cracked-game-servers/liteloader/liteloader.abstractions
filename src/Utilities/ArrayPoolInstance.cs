﻿using System;
using System.Collections.Generic;

namespace LiteLoader.Utilities
{
    internal sealed class ArrayPoolInstance
    {
        public readonly Type ArrayType;

        public readonly Array Empty;

        private readonly Queue<Array>[] _pools;

        public ArrayPoolInstance(Type arrayType)
        {
            ArrayType = arrayType ?? throw new ArgumentNullException(nameof(arrayType));
            _pools = new Queue<Array>[ArrayPool.MAX_ARRAY_LENGTH];
            Empty = Array.CreateInstance(ArrayType, 0);

            for (int i = 0; i < _pools.Length; i++)
            {
                _pools[i] = new Queue<Array>(ArrayPool.INITIAL_POOL_LENGTH);
                SetupArrays(i + 1, _pools[i]);
            }
        }

        public void FreeArray(Array array)
        {
            if (array == null || array.Length == 0 || array.GetType().GetElementType() != ArrayType) return;
            int arrayIndex = array.Length - 1;
            if (arrayIndex > _pools.Length) return;

            for (int i = 0; i < array.Length; i++)
            {
                array.SetValue(null, i);
            }

            Queue<Array> pool = _pools[arrayIndex];

            lock (pool)
            {
                if (pool.Count == ArrayPool.MAX_POOL_SIZE) return;
                if (pool.Count > ArrayPool.MAX_POOL_SIZE)
                {
                    int removeCount = pool.Count - ArrayPool.MAX_POOL_SIZE;
                    for (int i = 0; i < removeCount; i++) pool.Dequeue();
                    return;
                }

                pool.Enqueue(array);
            }
        }

        public Array GetArray(int length)
        {
            if (length == 0) return Empty;

            int arrayIndex = length - 1;

            if (arrayIndex > _pools.Length) return Array.CreateInstance(ArrayType, length);

            Queue<Array> pool = _pools[arrayIndex];

            lock (pool)
            {
                if (pool.Count == 0) SetupArrays(length, pool);

                return pool.Dequeue();
            }
        }

        private void SetupArrays(int length, Queue<Array> pool)
        {
            int count = pool.Count;

            if (count >= ArrayPool.INITIAL_POOL_LENGTH) return;

            count = ArrayPool.INITIAL_POOL_LENGTH - count;

            for (int i = 0; i < count; i++)
            {
                pool.Enqueue(Array.CreateInstance(ArrayType, length));
            }
        }
    }
}
