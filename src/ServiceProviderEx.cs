﻿using LiteLoader.DependencyInjection;
using System;

namespace LiteLoader
{
    public static class ServiceProviderEx
    {
        public static IDynamicServiceCollection AddSingleton<TService, TImplementation>(this IDynamicServiceCollection services)
        {
            services.Add(ServiceDescriptor.CreateSingleton(typeof(TService), typeof(TImplementation)));
            return services;
        }

        public static IDynamicServiceCollection AddSingleton<TService, TImplementation>(this IDynamicServiceCollection services, TImplementation instance)
        {
            services.Add(ServiceDescriptor.CreateSingleton(typeof(TService), typeof(TImplementation), instance));
            return services;
        }

        public static IDynamicServiceCollection AddSingleton<TService>(this IDynamicServiceCollection services)
        {
            services.Add(ServiceDescriptor.CreateSingleton(typeof(TService), (object)null));
            return services;
        }

        public static IDynamicServiceCollection AddSingleton<TService>(this IDynamicServiceCollection services, TService instance)
        {
            services.Add(ServiceDescriptor.CreateSingleton(typeof(TService), instance));
            return services;
        }

        public static IDynamicServiceCollection AddTransient<TService, TImplementation>(this IDynamicServiceCollection services)
        {
            services.Add(ServiceDescriptor.CreateTransient(typeof(TService), typeof(TImplementation)));
            return services;
        }

        public static IDynamicServiceCollection AddTransient<TService>(this IDynamicServiceCollection services)
        {
            services.Add(ServiceDescriptor.CreateTransient(typeof(TService)));
            return services;
        }

        public static TService GetService<TService>(this IServiceProvider provider) => (TService)provider.GetService(typeof(TService));
    }
}
