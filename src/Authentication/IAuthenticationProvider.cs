﻿namespace LiteLoader.Authentication
{
    public interface IAuthenticationProvider
    {
        /// <summary>
        /// Name of this authenticator
        /// </summary>
        string Name { get; }
    }
}
